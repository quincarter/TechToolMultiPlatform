/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package techtoolmultiplatform;

/**
 *
 * @author Quin
 */

import javax.swing.JOptionPane;
        
public class TechToolUIOSSelector extends javax.swing.JFrame {

    /**
     * Creates new form TechToolUIOSSelector
     */
    public TechToolUIOSSelector() {
        initComponents();
        setLocationRelativeTo(null);//centers window on the screen
        
    }//end constructor

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jOptionPane1 = new javax.swing.JOptionPane();
        OSDropdown = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        OSselectorOK = new javax.swing.JButton();
        OSselectorCancel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("OS Selection");
        setLocation(new java.awt.Point(0, 0));
        setResizable(false);

        OSDropdown.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        OSDropdown.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select an Operating System", "Windows", "Mac OS X" }));
        OSDropdown.setToolTipText("");
        OSDropdown.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OSDropdownActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel1.setText("Welcome to the Tech Tool Multiplatform!");

        OSselectorOK.setText("OK");
        OSselectorOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OSselectorOKActionPerformed(evt);
            }
        });

        OSselectorCancel.setText("Cancel");
        OSselectorCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OSselectorCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(124, 124, 124)
                .addComponent(OSDropdown, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(105, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(OSselectorOK, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(OSselectorCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel1))
                .addGap(96, 96, 96))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(OSDropdown, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(OSselectorOK, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
                    .addComponent(OSselectorCancel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void OSDropdownActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OSDropdownActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_OSDropdownActionPerformed

    private void OSselectorOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OSselectorOKActionPerformed
        // TODO add your handling code here:
        int userChoiceIndex = OSDropdown.getSelectedIndex();//getting the selection into a variable for validation
        
        switch (userChoiceIndex)//this will gather the choice from the dropdown box
        {
            case 0://Select an Operating System
                JOptionPane.showMessageDialog(null, "Please Choose a Valid Operating System\nfrom the dropdown list.", "Error",
                                    JOptionPane.ERROR_MESSAGE);//error message stating that they must choose something other than the first option
                break;//breaks out of code
            case 1://Windows
                String osName = System.getProperty("os.name");//gathering OS name
                if (osName.contains("Windows"))//validating OS
                        {
                            dispose();//closes the selection window
                            TechToolMultiPlatform.osIsWindows();
                            break;//breaks out of code
                        }
                else
                {
                    JOptionPane.showMessageDialog(null, "You are not running a version of Windows OS", "Error",
                                    JOptionPane.ERROR_MESSAGE);//error validation
                    break;//breaks out of code
                }
            case 2://Mac OS X
                osName = System.getProperty("os.name");//gathering OS name
                if (osName.contains("Mac"))//validating OS
                        {
                            TechToolMultiPlatform.osIsMac();
                            break;//breaks out of code. 
                        }
                else
                {
                    JOptionPane.showMessageDialog(null, "You are not running a version of Mac OS X", "Error",
                                    JOptionPane.ERROR_MESSAGE);//validation message.
                    break;//breaks out of code
                }
                
        }
    }//GEN-LAST:event_OSselectorOKActionPerformed

    private void OSselectorCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OSselectorCancelActionPerformed
        // TODO add your handling code here:
        System.exit(0);//closes the window. no cleanup
    }//GEN-LAST:event_OSselectorCancelActionPerformed

    /**
     * @param args the command line arguments
     */
    public void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TechToolUIOSSelector.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TechToolUIOSSelector.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TechToolUIOSSelector.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TechToolUIOSSelector.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */

        java.awt.EventQueue.invokeLater(new Runnable() 
        {
            public void run() 
            {                
                new TechToolUIOSSelector().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> OSDropdown;
    private javax.swing.JButton OSselectorCancel;
    private javax.swing.JButton OSselectorOK;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JOptionPane jOptionPane1;
    // End of variables declaration//GEN-END:variables
}
