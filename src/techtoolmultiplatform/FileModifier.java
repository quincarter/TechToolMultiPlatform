/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package techtoolmultiplatform;

/**
 *
 * @author Quin
 */
import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileModifier {

  public static boolean renameFileExtension
  (String source, String newExtension)
  {//begin rename extension
    String target;
    String currentExtension = getFileExtension(source);

    if (currentExtension.equals(""))
    {//checks to see if the current file has an extension
      target = source + "." + newExtension;
    }
    else 
    {//replaces the extension with the desired extension
      target = source.replaceFirst(Pattern.quote("." +
          currentExtension) + "$", Matcher.quoteReplacement("." + newExtension));

    }
    return new File(source).renameTo(new File(target));
  }//end rename extension

  public static String getFileExtension(String f) {
    String ext = "";
    int i = f.lastIndexOf('.');
    if (i > 0 &&  i < f.length() - 1) {
      ext = f.substring(i + 1);
    }
    return ext;
  }
    
}//end class
