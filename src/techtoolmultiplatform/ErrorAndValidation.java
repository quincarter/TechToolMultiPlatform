/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package techtoolmultiplatform;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import javax.swing.JOptionPane;

/**
 *
 * @author Quin
 */
public class ErrorAndValidation 
{//begin class
    public static String validationDecider(String scriptFilePath)
    {
        String filePathToValidate = null;
        String validationReturned = null;
        
        String OSName = System.getProperty("os.name");
        
        /*add more file types as they arrise here
        Keychains have been added for the keychain tool
        */
        
        if (scriptFilePath.contains("Keychains") && OSName.contains("Mac"))
        {//begin checking keychain command file lengths to determine what the file path needs to be
            String keychainsFilePathToValidate = null;
            
            keychainsFilePathToValidate = scriptFilePath.substring(19, scriptFilePath.length()-3); 
            //substring goes as: (start point, end point) this one has a minus 3 so it will back up 3 places after getting the full length
            filePathToValidate = keychainsFilePathToValidate;
        }//end checking keychain command file lengths to determine what the file path needs to be
        else if (scriptFilePath.contains("Genieo") && OSName.contains("Mac"))
        {
            
        } 
        
        if (OSName.contains("Windows"))
        {//begin validating windows
            filePathToValidate = scriptFilePath;
            validationReturned = windowsFileAndDirectoryValidation(filePathToValidate);
        }//end validating windows*/
        
        ///////DIRECTORY CHECKER\\\\\\\\\\
        if (filePathToValidate.contains("/*/") && OSName.contains("Mac"))
        {//this will check to see if any directories exist  
            validationReturned = directoryValidation(filePathToValidate); //validating the directory exists               
        }//end checking if any directories exist                         
        //////END DIRECTORY CHECKER

        /////////////FILE CHECKER\\\\\\\\\\\\
        else if (filePathToValidate.contains("login") && OSName.contains("Mac"))
        {
            validationReturned = fileValidation(filePathToValidate);//validating the file exists
        }
        else if (filePathToValidate.endsWith(".*") && OSName.contains("Mac"))
        {
            validationReturned = fileValidation(filePathToValidate);//validating the file exists
        }

        else if (filePathToValidate.endsWith(".app") && OSName.contains("Mac"))
        {
            validationReturned = fileValidation(filePathToValidate);//validating the file exists
        }

        ///////////END FILE CHECKER\\\\\\\\\\\\
        
        return validationReturned;
    }//end validation decider
    
    public static void errorCodes (int errorCode)
    {//begin error codes     
        if (errorCode == 0)
        {//checking error code value
            JOptionPane.showMessageDialog(null, "Command Was A Success!", "Success!",
                                JOptionPane.INFORMATION_MESSAGE);//Success message                
        }//end if
        else if (errorCode == 1)
        {//begin elseif and checking error code value
            JOptionPane.showMessageDialog(null, "Error. Cannot write tool file to the /tmp directory.", "Error",
                                JOptionPane.ERROR_MESSAGE);//error message
        }//end elseif and checking error code value

        else if (errorCode == 2)
        {//begin file or directory doesn't exist
            JOptionPane.showMessageDialog(null, "The File Or Directory that the script is pointing to doesn't seem to exist....", "Error",
                                JOptionPane.ERROR_MESSAGE);//error message                
        }//end file or directory doesn't exist

        else if (errorCode == 3)
        {//begin The command did not run properly...
            JOptionPane.showMessageDialog(null, "An Error Occurred when trying to VALIDATE the directory or file exists...", "Error",
                                JOptionPane.ERROR_MESSAGE);//error message                
        }//end The command did not run properly...
        
        else if (errorCode == 4)
        {//Network Error. Could not download the files from the network location
            JOptionPane.showMessageDialog(null, "Network Error. Could not download the files from the network location", "Error",
                                JOptionPane.ERROR_MESSAGE);//error message 
        }//Network Error. Could not download the files from the network location
        
        else if (errorCode == 5)
        {//File Execution Error. Could not execute the specified file
            JOptionPane.showMessageDialog(null, "File Execution Error. Could not execute the specified file", "Error",
                                JOptionPane.ERROR_MESSAGE);//error message 
        }//File Execution Error. Could not execute the specified file
        
        else if (errorCode == 100)
        {//nothing will occur here
            //this is used for the items creation. if no error occurs then the user will not be notified
        }//nothing will occur here
        
        else if (errorCode == 101)
        {//File Execution Error. Could not execute the specified file
            JOptionPane.showMessageDialog(null, "Failed to run command to delete remaining scripts...", "Error",
                                JOptionPane.ERROR_MESSAGE);//error message 
        }//File Execution Error. Could not execute the specified file
    }//end error codes
    
    
    public static String directoryValidation (String filePathToValidate)
    {//begin directory validation        
        //setting variables to pass into the file creation method in the creator class
        
        String toolName = "directoryValidator";                                
        String scriptCommand = "[ -d " + filePathToValidate + " ] && echo \"Exists\" || echo \"Doesn't\"";
        String toolFileName = "/tmp/" + toolName + ".sh";//creating the file name that will be created in /tmp/        
        String validationMessage = null;
        
        try
        {//begin try
            Runtime.getRuntime().exec("chmod 777 " + toolFileName);  //setting file permissions to read/write/execute
            Writer output = new BufferedWriter(new FileWriter(toolFileName));//creating the output object for writing to the sh file
            output.write(scriptCommand);//writing information stored in the comined variable for the shell script
            output.close();//closing output
            
            ProcessBuilder pb = new ProcessBuilder(toolFileName);//creating process builder object to create the new process of our new file name
            Process p = pb.start();//starting the process that was just created
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));//reading the input from the process
                        
            String line = null;//setting line to null
            while ((line = reader.readLine()) != null)
            {//begin while
                validationMessage = line;//storing either Exists or Doesn't
            }//end while
            
            return validationMessage;//returning this message to the previous class
        }//end try
        
        catch(IOException ex)//catching any IOExceptions that may occur when running the process, processbuilder, bufferedreader, inputstreamreader, or runtime objects
        {//begin catch 
            validationMessage = "Command Did not Run";
            return validationMessage;
        } //end catch         
        
    }//end directory validation
    
    public static String fileValidation (String filePathToValidate)
    {//begin file validation        
        //setting variables to pass into the file creation method in the creator class
        String toolName = "fileValidator";
        String scriptCommand = "[ -e " + filePathToValidate + " ] && echo \"Exists\" || echo \"Doesn't\"";
        String toolFileName = "/tmp/" + toolName + ".sh";//creating the file name that will be created in /tmp/        
        String validationMessage = null;
        
        try
        {//begin try
            Runtime.getRuntime().exec("chmod 777 " + toolFileName);  //setting file permissions to read/write/execute            
            Writer output = new BufferedWriter(new FileWriter(toolFileName));//creating the output object for writing to the sh file
            output.write(scriptCommand);//writing information stored in the comined variable for the shell script
            output.close();//closing output
            
            ProcessBuilder pb = new ProcessBuilder(toolFileName);//creating process builder object to create the new process of our new file name
            Process p = pb.start();//starting the process that was just created
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));//reading the input from the process
                        
            String line = null;//setting line to null
            while ((line = reader.readLine()) != null)
            {//begin while
                validationMessage = line;//storing either Exists or Doesn't
            }//end while
            
            return validationMessage;//returning this message to the previous class
        }//end try
        
        catch(IOException ex)//catching any IOExceptions that may occur when running the process, processbuilder, bufferedreader, inputstreamreader, or runtime objects
        {//begin catch 
            validationMessage = "Command Did not Run";
            return validationMessage;
        } //end catch         
        
    }//end directory validation 
    
    public static String windowsFileAndDirectoryValidation (String filePathToValidate)
    {//begin windows validation
        String validationMessage = null;
        String newExtension = "bat";
        String source = "C:\\TechTool\\windowsFileChecker.txt";
        String windowsFileChecker = "IF EXIST " + filePathToValidate + " (\n" +
                                                    "echo exists" +
                                                    ") ELSE (\n" +
                                                    "echo doesn't) \n exit";
        
        try
        {//begin try
            Writer output = new BufferedWriter(new FileWriter(source));
            output.write(windowsFileChecker);
            output.close();
            //Runtime.getRuntime().exec("chmod 777 /tmp/toast.sh");
            FileModifier.renameFileExtension(source, newExtension);

            Process p = Runtime.getRuntime().exec("cmd /c start C:\\TechTool\\windowsFileChecker.bat");        

            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null)
            {                
               validationMessage = line;
               System.out.println(validationMessage);
            }
            return validationMessage;
        }//end try
        catch (IOException ex)
        {
            validationMessage = "Command Did not Run";
            return validationMessage;
        }
    }//end windows validation
}//end class
