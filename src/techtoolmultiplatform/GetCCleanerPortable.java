/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package techtoolmultiplatform;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Writer;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbFileOutputStream;;

/**
 *
 * @author Quin
 */
public class GetCCleanerPortable 
{//begin class
    public static int getCCleanerPortable(String networkPath, String destinationPath)
    {//begin file copy method 
        //String user = "user:password";
        //NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(user);
        //String path = "smb://my_machine_name/D/MyDev/test.txt";
        //SmbFile sFile = new SmbFile(path, auth);
        //SmbFileOutputStream sfos = new SmbFileOutputStream(sFile);
        //sfos.write("Test".getBytes());
        
        int errorCode = 0;
        try 
        {//begin try
            File srcFile = new File(networkPath);
            File dstFileName = new File(destinationPath);
            
            //create output directory if it doesn't exist
            File folder = new File("C:\\TechTool\\");
            
            if(!folder.exists())
            {
    		folder.mkdirs();
            }
            
            
            String dstFileNameString = "C:\\TechTool\\ccsetup511.zip";
            if(!dstFileName.exists())
            {
                dstFileName.createNewFile();
            }
            else
            {
                dstFileName.getParentFile().mkdirs();
            }
            InputStream in;
            in = new FileInputStream(srcFile);
            OutputStream out;
            out = new FileOutputStream(dstFileName);

            byte[] buff = new byte[1024];
            int length;

            while ((length = in.read(buff)) > 0) 
            {
                out.write(buff, 0, length);
            }
                in.close();
                out.close();
                
            Unzip uz = new Unzip();
        
            String outputFolder = dstFileName.toString();

            outputFolder = outputFolder.substring(0, outputFolder.length()-4);
            System.out.println(outputFolder);
            System.out.println(dstFileName);                        
            
            uz.unZipIt(dstFileNameString, outputFolder);
            errorCode = 100;//setting to 100 so no pop up box happens
        }//end try 
        catch (Exception ex) 
        {//begin catch
            errorCode = 4;//network error code
            return errorCode;
        }   //end catch
        createCCleanerBat();
        return errorCode;
    }//end file copy method
    
    public static int createCCleanerBat ()
    {
        int errorCode = 0;
        String newExtension = "bat";
        String source = "C:\\TechTool\\runCCleaner.txt";
        String writeCCleanerRunStatement = 
                "@echo off \n "
                + "C:\\TechTool\\ccsetup511\\ccsetup511\\CCleaner64.exe"
                + " \n cls "
                + "\n exit";
        try
        {//begin try
            Writer output = new BufferedWriter(new FileWriter(source));
            output.write(writeCCleanerRunStatement);
            output.close();
            //Runtime.getRuntime().exec("chmod 777 /tmp/toast.sh");
            FileModifier.renameFileExtension(source, newExtension);
            
            errorCode = 100;
        }//end try//end try//end try//end try
        catch (IOException ex)
        {
            errorCode = 6;
            return errorCode;
        }
        
        return errorCode;
    }
}//end class
