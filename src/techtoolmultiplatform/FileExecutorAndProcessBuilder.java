/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package techtoolmultiplatform;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Quin
 */
public class FileExecutorAndProcessBuilder 
{//begin class
    public static int fileExecutor(String toolFileName, String scriptCommand)
    {//begin file executor
        ErrorAndValidation ev = new ErrorAndValidation();
        String OSName = System.getProperty("os.name");
        
        int errorCode = 0;
        
        String validationReturned = null;        
        
        if (!toolFileName.endsWith(".exe"))
        {
            validationReturned = "exists";
        }

        else
        {
            validationReturned = ev.validationDecider(scriptCommand);
        }
        if (validationReturned.equalsIgnoreCase("exists"))
        {//begin if ---- only executes if the file exists
            //if (OSName.contains("Mac"))
            //{
                try 
                {//begin try

                    /////////////EXECUTES THE FILE\\\\\\\\\\\\\\\\\ (WORKS WITH EXE, SH, BAT, ANY FILE THAT CAN BE EXECUTED)
                    ProcessBuilder pb = new ProcessBuilder(toolFileName);//creating process builder object to create the new process of our new file name
                    Process p = pb.start();//starting the process that was just created
                    BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));//reading the input from the process

                    String line = null;//setting line to null
                    while ((line = reader.readLine()) != null)
                    {
                        System.out.println(line);
                    } 

                    if (toolFileName.contains("deleteScripts"))
                    {//setting error code to 100 so it will not show cleanup message
                        errorCode = 100;
                    }//setting error code to 100 so it will not show cleanup message
                    
                    else if (toolFileName.endsWith(".exe"))
                    {
                        errorCode = 100;
                    }
                    else
                    {//shows success message after returning
                        errorCode = 0;
                    }//shows success message after returning

                } //end try
                catch (IOException ex) 
                {//begin catch
                    if (toolFileName.contains("deleteScripts"))
                    {//setting error code to 101 so it will not show cleanup message
                        errorCode = 101;
                    }//setting error code to 101 so it will not show cleanup message

                    else
                    {//shows success message after returning
                        errorCode = 5;//File Execution error code
                    }//shows success message after returning
                    Logger.getLogger(FileExecutorAndProcessBuilder.class.getName()).log(Level.SEVERE, null, ex);
                    return errorCode;
                }//end catch                                
           // }
            
            /*
            else if (OSName.contains("Windows"))
            {
                try 
                {
                    //begin execute
                    Runtime.getRuntime().exec("cmd /c start " + toolFileName);
                    errorCode = 100;//wont return a success message
                } 

                catch (IOException ex) 
                {
                    errorCode = 5;//file execution error
                    Logger.getLogger(TechToolMultiPlatform.class.getName()).log(Level.SEVERE, null, ex);                
                }

            }*/
        }//end if ---only executes if the file exists 
                    
        else if (validationReturned.equalsIgnoreCase("doesn't"))
        {//begin else if
            errorCode = 2;//File doesn't exist error code
        }//end else if

        else if (validationReturned.equalsIgnoreCase("command did not run"))
        {//begin else if command did not run
            errorCode = 3;//command did not run error code. 
        }//end else if command did not run
        
        return errorCode;//returning error code to source
                
    }//end file executor
}//end class
