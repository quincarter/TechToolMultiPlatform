/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package techtoolmultiplatform;

/**
 *
 * @author Quin Carter
 * Tech Toolkit beta
 * 
 * Writing began on 11/10/2015
 * 
 * The purpose of this application is to provide tools and options
 * for technicians to give them a multipurpose tool that will fix a 
 * wide variety of issues. 
 * 
 */
import java.io.BufferedWriter;
import java.io.Writer;
import java.io.BufferedReader;
import java.util.*;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.PrintStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TechToolMultiPlatform {//begin class

    /**
     * @param args the command line arguments
     */            
    private static String toolName = null;
    private static String scriptCommand = null;
    private static String toolFileName = null;
    private static String scriptCommandCombined = null;
    private static String networkPath = null;
    private static String destinationPath = null;
    
    public static void main(String[] args) 
    {//begin main
        // TODO code application logic here
        
        /*The following block of code determines the current operating system
        that the user is using. This will determine which tools populate on
        the screen. This is done so people will not use windows tools on Mac 
        and visa-versa.
        */
        
        //Begin OS validation
        String osName = System.getProperty("os.name");//gathering OS name
        
        if (osName.contains("Windows"))
        {//checks to see if the current OS is Windows and will auto advance to the correct tools
            osIsWindows();//calls the windows method
        }//end if
        else if (osName.contains("Mac"))
        {//checks to see if the current OS is Mac and will auto advance to the correct tools
            osIsMac();//calls the mac method
        }//end else if
        else
        {//if the program fails to grab an OS name, it will go to the selector
            TechToolUIOSSelector selector = new TechToolUIOSSelector();
            selector.setVisible(true);
        }//end else
    }//end main
    
    public static void osIsWindows()
    {//begin OS is Windows
        String create = "create";
        TechToolUIWindows windows = new TechToolUIWindows();//creating a new object of the windows tools class
        
        ccleanerPortable(create);        
        
        windows.setVisible(true);//setting the windows tools to the primary window
    }//End OS is Windows
    
    public static void osIsMac()
    {//begin OS is Mac
        String create = "create";
        //write files to temp folder        
        localItemsKeychain(create);
        loginKeychain(create);
        allItemsKeychain(create);
        
        ////IMPORTANT -- MUST ADD TO THIS TO DELETE THE ABOVE SCRIPTS\\\\\
        deleteScripts(create);
        
        
        TechToolUIMac mac = new TechToolUIMac();                
        mac.setVisible(true);
    }//end OS is Mac
    
    /*
    MAC BASH SCRIPTS BELOW
    
    toolName IS USED TO NAME THE SH FILE
    scriptCommand IS USED TO VALIDATE THAT THE AFFECTED FILE OR DIRECTORY EXISTS
    toolFileName BUILDS THE FILENAME WITH THE SPECIFIED TOOL NAME
    scriptCommandCombined BUILDS THE FULL SCRIPT THAT WILL GO INTO THE .SH FILE
    
    
    The following methods are for writing the bat and bash files to the temporary
    directory on the host machine
    
    BE SURE TO CALL THE FOLLOWING METHODS IN osIsMac() ABOVE WITH THE 
    CREATE VARIABLE PASSED IN SO THAT THE SHELL SCRIPTS ARE CREATED.
    */
    
    public static void localItemsKeychain(String createOrExecute)
    {//begin local items keychain creation
        MacBashFileCreator fc = new MacBashFileCreator();//creating file creator object
        FileExecutorAndProcessBuilder pb = new FileExecutorAndProcessBuilder();
        
        /////Declaring Variables\\\\\\
        int errorCode = 0;
        
        ////////Change these variables to make the script do what you want            
        toolName = "localItems";//this command will be passed into the file creator
        scriptCommand = "#!/bin/bash \nrm -rf ~/Library/Keychains/*/ > ";//this command will be passed into the file creator                                            
        toolFileName = "/tmp/" + toolName + ".sh";//creating the file name that will be created in /tmp/
        scriptCommandCombined = scriptCommand + "/tmp/" + toolName + ".txt";//combining the script command with the toolName
                
        if (createOrExecute.equalsIgnoreCase("create"))
        {//begin creation
            /////////////////////////CREATING THE SHELL SCRIPT FILE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            errorCode = fc.fileCreator(toolName, scriptCommand);//calling the local items keychain method within the bashfile creator class
            ErrorAndValidation.errorCodes(errorCode);//will show success or error in a pop up box 
        }//end creation
        
        else if (createOrExecute.equalsIgnoreCase("execute"))
        {//begin execute
            errorCode = pb.fileExecutor(toolFileName, scriptCommand);
            ErrorAndValidation.errorCodes(errorCode);//will show success or error in a pop up box 
        }//end execute
    }//end local items keychain creation
    
    public static void loginKeychain(String createOrExecute)
    {//begin login items keychain creation
        MacBashFileCreator fc = new MacBashFileCreator();//creating file creator object
        FileExecutorAndProcessBuilder pb = new FileExecutorAndProcessBuilder();
        
        /////Declaring Variables\\\\\\
        int errorCode = 0;
        
        ////////Change these variables to make the script do what you want
        toolName = "loginKeychain";//this command will be passed into the file creator
        scriptCommand = "#!/bin/bash \nrm -rf ~/Library/Keychains/login* > ";//this command will be passed into the file creator                      
        toolFileName = "/tmp/" + toolName + ".sh";//creating the file name that will be created in /tmp/
        scriptCommandCombined = scriptCommand + "/tmp/" + toolName + ".txt";//combining the script command with the toolName
                
        if (createOrExecute.equalsIgnoreCase("create"))
        {//begin creation
            /////////////////////////CREATING THE SHELL SCRIPT FILE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            errorCode = fc.fileCreator(toolName, scriptCommand);//calling the local items keychain method within the bashfile creator class
            ErrorAndValidation.errorCodes(errorCode);//will show success or error in a pop up box 
        }//end creation
        
        else if (createOrExecute.equalsIgnoreCase("execute"))
        {//begin execute
            errorCode = pb.fileExecutor(toolFileName, scriptCommand);
            ErrorAndValidation.errorCodes(errorCode);//will show success or error in a pop up box 
        }//end execute
    }//end login items keychain creation
    
    public static void allItemsKeychain(String createOrExecute)
    {//begin all items keychain creation
        MacBashFileCreator fc = new MacBashFileCreator();//creating file creator object
        FileExecutorAndProcessBuilder pb = new FileExecutorAndProcessBuilder();
        
        /////Declaring Variables\\\\\\
        int errorCode = 0;
        
        ////////Change these variables to make the script do what you want
        toolName = "clearAllKeychains";//this command will be passed into the file creator
        scriptCommand = "#!/bin/bash \nrm -rf ~/Library/Keychains/* > ";//This command will be passed into the file creator
        toolFileName = "/tmp/" + toolName + ".sh";//creating the file name that will be created in /tmp/
        scriptCommandCombined = scriptCommand + "/tmp/" + toolName + ".txt";//combining the script command with the toolName
                
        if (createOrExecute.equalsIgnoreCase("create"))
        {//begin creation
            /////////////////////////CREATING THE SHELL SCRIPT FILE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            errorCode = fc.fileCreator(toolName, scriptCommand);//calling the local items keychain method within the bashfile creator class
            ErrorAndValidation.errorCodes(errorCode);//will show success or error in a pop up box 
        }//end creation
        
        else if (createOrExecute.equalsIgnoreCase("execute"))
        {//begin execute
            errorCode = pb.fileExecutor(toolFileName, scriptCommand);
            ErrorAndValidation.errorCodes(errorCode);//will show success or error in a pop up box 
        }//end execute
    }//end all items keychain creation
    
    
    /*
    ADD TO THE BELOW scriptCommand 
    WHEN MORE SCRIPTS ARE CREATED TO REMOVE THE OTHER FILES
    */
    public static void deleteScripts(String createOrExecute)
    {//begin delete scripts creation
        MacBashFileCreator fc = new MacBashFileCreator();//creating file creator object
        FileExecutorAndProcessBuilder pb = new FileExecutorAndProcessBuilder();
        
        /////Declaring Variables\\\\\\
        int errorCode = 0;
        
        ////////Change these variables to make the script do what you want
        toolName = "deleteScripts";//this command will be passed into the file creator
        scriptCommand = "#!/bin/bash \nrm -rf /tmp/localItems*\nrm -rf /tmp/login*\nrm -rf clearAllKeychains* > ";//This command will be passed into the file creator
        
        toolFileName = "/tmp/" + toolName + ".sh";//creating the file name that will be created in /tmp/
        scriptCommandCombined = scriptCommand + "/tmp/" + toolName + ".txt";//combining the script command with the toolName
                
        if (createOrExecute.equalsIgnoreCase("create"))
        {//begin creation
            /////////////////////////CREATING THE SHELL SCRIPT FILE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            errorCode = fc.fileCreator(toolName, scriptCommand);//calling the local items keychain method within the bashfile creator class
            ErrorAndValidation.errorCodes(errorCode);//will show success or error in a pop up box 
        }//end creation
        
        else if (createOrExecute.equalsIgnoreCase("execute"))
        {//begin execute
            errorCode = pb.fileExecutor(toolFileName, scriptCommand);
            ErrorAndValidation.errorCodes(errorCode);//will show success or error in a pop up box 
        }//end execute
    }//end all items keychain creation
    
    public static void getMalwareBytesMac(String createOrExecute)
    {//begin getting malware bytes for mac
        FileExecutorAndProcessBuilder pb = new FileExecutorAndProcessBuilder();

        
        /////Declaring Variables\\\\\\
        int errorCode = 0;               
        
        ////////PRE-DOWNLOAD AND PRE-DECOMPRESS
        networkPath = "\\\\nas2.lisd.local\\Technology\\Tech\\techsrv\\PC\\Tools\\Clean\\CCleanerPortable511.zip";//creating the file name that will be created in /tmp/
        destinationPath = "C:\\TechTool\\CCleanerPortable511.zip";//combining the script command with the toolName
        
        ////POST ZIP -- EXECUTE ONLY\\\\\\
        toolFileName = "C:\\TechTool\\runCCleaner.bat";//name of the tool
        scriptCommand = "C:\\TechTool\\CCleanerPortable511\\ccsetup511\\CCleaner.exe";//in this instance, we are checking to see if this particular file exists. 
        if (createOrExecute.equalsIgnoreCase("create"))
        {//begin creation
            /////////////////////////DOWNLOADING THE ZIP AND UNZIPPING THE FILES\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ 
            GetCCleanerPortable ccleaner = new GetCCleanerPortable();            
            errorCode = ccleaner.getCCleanerPortable(networkPath, destinationPath);//downloading from the network drive            
            ErrorAndValidation.errorCodes(errorCode);//will show success or error in a pop up box 
        }//end creation
        
        else if (createOrExecute.equalsIgnoreCase("execute"))
        {   
           errorCode = pb.fileExecutor(toolFileName, scriptCommand);
           ErrorAndValidation.errorCodes(errorCode);//will show success or error in a pop up box            
        }//end execute      
    }//end getting malware bytes for mac
    
    
    
    
    /*
    WINDOWS OS METHODS
    
    WINDOWS FILES ARE DESIGNED DIFFERENTLY
    THE FILES MUST BE DOWNLOADED FROM A NETWORK LOCATION
    OR THE INTERNET.
    
    
    IN ORDER TO MAKE CHANGES, A NEW METHOD MUST BE CREATED IN THE GetCCleanerPortable.java class
    
    TO EXECUTE THE DOWNLOAD:
    CREATE AN OBJECT OF THE GetCCleanerPortable CLASS
    CALL THE DESIRED METHOD
    
    METHOD MUST BE CALLED ABOVE IN THE OsIsWindows METHOD WITH THE CREATE VARIABLE PASSED IN 
    */
    public static void ccleanerPortable(String createOrExecute)
    {//begin ccleaner portable
        FileExecutorAndProcessBuilder pb = new FileExecutorAndProcessBuilder();

        
        /////Declaring Variables\\\\\\
        int errorCode = 0;               
        
        ////////PRE-DOWNLOAD AND PRE-DECOMPRESS
        networkPath = "\\\\nas2.lisd.local\\Technology\\Tech\\techsrv\\PC\\Tools\\Clean\\CCleanerPortable511.zip";//creating the file name that will be created in /tmp/
        destinationPath = "C:\\TechTool\\CCleanerPortable511.zip";//combining the script command with the toolName
        
        ////POST ZIP -- EXECUTE ONLY\\\\\\
        toolFileName = "C:\\TechTool\\runCCleaner.bat";//name of the tool
        scriptCommand = "C:\\TechTool\\CCleanerPortable511\\ccsetup511\\CCleaner.exe";//in this instance, we are checking to see if this particular file exists. 
        if (createOrExecute.equalsIgnoreCase("create"))
        {//begin creation
            /////////////////////////DOWNLOADING THE ZIP AND UNZIPPING THE FILES\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ 
            GetCCleanerPortable ccleaner = new GetCCleanerPortable();            
            errorCode = ccleaner.getCCleanerPortable(networkPath, destinationPath);//downloading from the network drive            
            ErrorAndValidation.errorCodes(errorCode);//will show success or error in a pop up box 
        }//end creation
        
        else if (createOrExecute.equalsIgnoreCase("execute"))
        {   
           errorCode = pb.fileExecutor(toolFileName, scriptCommand);
           ErrorAndValidation.errorCodes(errorCode);//will show success or error in a pop up box            
        }//end execute        
    }//end ccleaner portable
}//end class
