/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package techtoolmultiplatform;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import javax.swing.JOptionPane;

/**
 *
 * @author Quin
 */
public class MacBashFileCreator 
{//begin class    
    public int fileCreator(String toolName, String scriptCommand)//passing in two variables
    {//begin file creator method
        String validationReturned = null;
        
        int errorCode = 0;//initiallizing the int variable errorCode to 0
        String scriptCommandCombined = scriptCommand + "/tmp/" + toolName + ".txt";//combining the script command with the toolName
        String toolFileName = "/tmp/" + toolName + ".sh";//creating the file name that will be created in /tmp/
        
            try
            {//begin try
                Writer output = new BufferedWriter(new FileWriter(toolFileName));//creating the output object for writing to the sh file
                output.write(scriptCommandCombined);//writing information stored in the comined variable for the shell script
                output.close();//closing output           

                //This block of code executes the shell script\\ 
                Runtime.getRuntime().exec("chmod 777 " + toolFileName);  //setting file permissions to read/write/execute

                errorCode = 100;//setting errorCode to 100                
            }//end try
        
            catch(IOException ex)//catching any IOExceptions that may occur when running the process, processbuilder, bufferedreader, inputstreamreader, or runtime objects
            {//begin catch
                errorCode = 1;//setting error code to 1
                return errorCode;//returning error code of 1 signifying that there was at least 1 error that occurred. 
            } //end catch 
        
        return errorCode;//returning error code to the source
    }//end file creator method        

}//end class   
    
    
    
    
    
    
/////////////////////Created this block of code for individual method calls before using the variable system
////////////////////this is no longer used, but kept for testing purposes.
    
    /*
    public int localItemsCreationAndRun ()
    {//begin local items creation
        int errorCode = 0;
        try
        {
            Writer output = new BufferedWriter(new FileWriter("/tmp/localItems.sh"));
            output.write(localItems);
            output.close();
            Runtime.getRuntime().exec("chmod 777 /tmp/localItems.sh");  
              
            //This block of code executes the shell script 
            ProcessBuilder pb = new ProcessBuilder("/tmp/localItems.sh");
            Process p = pb.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = null;

            while ((line = reader.readLine()) != null)
            {
                System.out.println(line);
            } 
            
            errorCode = 0;
            return errorCode;                        
        }
        catch(IOException ex)
        {//begin catch
            errorCode = 1;
            return errorCode;
        } //end catch 
        
    }//end local items creation
    
    public int loginKeychainCreationAndRun ()
    {//begin local items creation
        int errorCode = 0;
        
        try
        {
            Writer output = new BufferedWriter(new FileWriter("/tmp/loginKeychain.sh"));
            output.write(loginKeychain);
            output.close();
            Runtime.getRuntime().exec("chmod 777 /tmp/loginKeychain.sh");  
              
            //This block of code executes the bash file
            ProcessBuilder pb = new ProcessBuilder("/tmp/loginKeychain.sh");
            Process p = pb.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = null;

            while ((line = reader.readLine()) != null)
            {
                System.out.println(line);
            } 
            errorCode = 0;
            return errorCode;
            
        }
        catch(IOException ex)
        {//begin catch
            errorCode = 1;
            return errorCode;
        } //end catch 
        
    }//end login keychain creation    
    
    public int allKeychainsCreationAndRun ()
    {//begin local items creation
        int errorCode = 0;
        
        try
        {
            Writer output = new BufferedWriter(new FileWriter("/tmp/allKeychains.sh"));
            output.write(allKeychains);
            output.close();
            Runtime.getRuntime().exec("chmod 777 /tmp/allKeychains.sh");  
              
            //This block of code executes the bash file
            ProcessBuilder pb = new ProcessBuilder("/tmp/allKeychains.sh");
            Process p = pb.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = null;

            while ((line = reader.readLine()) != null)
            {
                System.out.println(line);
            } 
            errorCode = 0;
            return errorCode;
            
        }
        catch(IOException ex)
        {//begin catch
            errorCode = 1;
            return errorCode;
        } //end catch 
        
    }//end login keychain creation 
    
  */
    /////////////////////Created this block of code for individual method calls before using the variable system
    ////////////////////this is no longer used, but kept for testing purposes.
    

