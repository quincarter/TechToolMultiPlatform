/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package techtoolmultiplatform;

import java.io.IOException;
import javax.swing.Timer;

/**
 *
 * @author Quin
 */
public class ShutdownCommand 
{
   public static void shutdown() throws RuntimeException, IOException 
   {
        String shutdownCommand;
        String operatingSystem = System.getProperty("os.name");

        if ("Linux".equals(operatingSystem) || "Mac OS X".equals(operatingSystem)) 
        {
            shutdownCommand = "shutdown -r 120 \"System needs to restart for changes to take effect\"";
        }
        
        else if ("Windows".equals(operatingSystem)) 
        {
            shutdownCommand = "shutdown.exe -r -t 120";
        }
        
        else 
        {
            throw new RuntimeException("Unsupported operating system.");
        }

        Runtime.getRuntime().exec(shutdownCommand);
        System.exit(0);
    }
}
